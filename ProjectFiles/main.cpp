/*****************************************************************//**
 * \file   main.cpp
 * \brief  This program is a todo task list and it stores tasks with
 *         user input information. You can change all task information
 *         after you have created it, you can delete it or view it.
 *         Tasks are saved when quitting the program
 * 
 * \author Aleksander Einarsen, Rolf Olav Velten, NTNU
 * \date   April 2021
 *********************************************************************/


#include <iostream>              //  cout, cin
#include <fstream>               //  ifstream, ofstream
#include <string>                
#include <vector>
#include <limits>                //  numeric_limits
#include "LesData2.h"            //  Toolkit for reading data
using namespace std;

/**
 *  Task (with priority, name, date, status and description).
 */
class Task
{
  private:
      int oppgavePrioritet; // Number that indicates priority
      string oppgaveNavn;   // Name of the task
      int dag, maaned, aar; // Day, month, year
      int oppgaveStatus;    // Status (finished, in progress, not startet)
      string oppgaveBeskrivelse; // Description

  public:
      Task() ///< Constructor without parameters
      {
          oppgavePrioritet = 0;
          oppgaveNavn = "";
          dag = maaned = aar = 0;
          oppgaveStatus = 0;
          oppgaveBeskrivelse = "";
      }

      Task(ifstream & inn); ///< Constructor with parameter input file
      virtual void endreData(); ///< Change task data
      virtual void lesData(); ///< Reads data from the user
      virtual void skrivData() const; ///< Shows data on screen
      virtual void skrivTilFil(ofstream & ut) const; ///< Writes data to a file
      virtual void settDato(); ///< Sets task date
      void settNavn(); ///< Set task name
      void settPrioritet(); ///< Set task priority
      void settStatus(); ///< Set task status
      void settBeskrivelse(); ///< Set task description

      void skrivNavn(); ///< Writes the name of the task on screen
      void skrivPrioritet() const; ///< Writes the priority of the task
      void skrivStatus() const; ///< Writes the status if the task

      void endreNavn(); ///< Changes task name
      void endrePrioritet(); ///< Changes priority
      void endreDato(); ///< Changes date
      void endreStatus(); ///< Changes status
      void endreBeskrivelse(); ///< Changes description

      string hentPrioritetNavn() const; ///< Returns priority name
      ///< Returns priority name of number
      string hentPrioritetNavn(const int i) const; 
      int hentPrioritetNummer(); ///< Returns priority number

      string hentStatusNavn() const; ///< Returns status name
      ///< returns status name from number
      string hentStatusNavn(const int i) const;
};

void endreEnOppgave(); ///< Changes a task
void lesFraFil(); ///< Reads data from file
void nyOppgave(); ///< Creates new task
void skrivAlleOppgaver(); ///< Writes tasks depenendt on user choice
void skrivUtAlleOppgaver(); ///< Writes all tasks
void skrivSorteringsMeny(); ///< Meny that shows how you can sort
 /// Writes tasks of a cirtan priority
void skrivUtMedPrioritetfilter(const int valg);
void skrivMeny(); ///< Writes main menu
void skrivEndreDataMeny(); ///< Menu for what to edit on a task
void skrivTilFil(); ///< Writes to file
void slettOppgave(); ///< Deletes selected task
void pressEnter(); ///< Press enter to continue
void listOppgaverForenklet(); ///< Prints task number and name
bool confirmDeletion(); ///< Gives a confirm message

//Not in use anymore
int sisteNr = 0;

vector <Task*> gOppgaver;    ///<  Datastrukturen med alle oppgavene.

/**
 * Main program.
 * 
 */
int main(void)
{
    lesFraFil(); // Reads data from file

    // Welcome text
    cout << "Hello, and welcome to Task List, a todo list application :)"
        << '\n';
    cout << "\nYou currently have " << gOppgaver.size()
        << " tasks registered.\n" << '\n';

    skrivMeny();
    char kommando = lesChar("Command"); // Takes a char from the user

    while (kommando != 'Q') {
        switch (kommando) {
          case 'N':  nyOppgave();           break; // New task
          case 'V':  skrivAlleOppgaver();   break; // Writes tasks
          case 'E':  endreEnOppgave();      break; // Change a task
          case 'D':  slettOppgave();        break; // Delete a task
          default:   cout << "Please enter a valid input\n\n"; break;
        }
        skrivMeny();
        kommando = lesChar("\nCommand"); // Takes a char from the user
    }
    skrivTilFil(); // Writes to a file
    return 0;
}

/**
 * Reads task data from user.
 * 
 * @see - settNavn()
 * @see - settPrioritet()
 * @see - settDato()
 * @see - settStatus()
 * @see - settBeskrivelse()
 */
void Task::lesData()
{
    settNavn(); // Sets a name from user
    settPrioritet(); // Sets a priority from user
    settDato(); // Sets a date from user
    settStatus(); // Sets a status from user
    settBeskrivelse(); // Sets a description from user
}

/**
 * Sets a task name from user.
 */
void Task::settNavn()
{
    cout << "Write a name and press ENTER\n" << '\n';
    cout << "Task Name: "; getline(cin, oppgaveNavn);
    cout << '\n';
}

/**
 * Sets a task priority from user.
 */
void Task::settPrioritet()
{
    cout << "Select a priority and press ENTER\n" << '\n';
    cout << "Priority:  " << '\n';
    cout << "\t1) High" << '\n';
    cout << "\t2) Medium" << '\n';
    cout << "\t3) Low" << '\n';
    cout << '\n';
    oppgavePrioritet = lesInt("Command", 1, 3);
    cout << '\n';
    skrivPrioritet(); // Writes the current priority
}

/**
 * Sets a task date from user.
 */
void Task::settDato()
{
    cout << "\nWrite a date and press ENTER\n" << '\n';
    dag = lesInt("\tDay", 1, 31);
    maaned = lesInt("\tMoth", 1, 12); //Todo implementer måned klasse for å forhindre å skrive inn flere dager enn det er i måneden
    aar = lesInt("\tYear", 2000, 3000); //Todo get current day
    cout << '\n';

    //Feedback to the user
    cout << "\nDate set to " << dag << "." << maaned << "." << aar
         << "\n" << '\n';
}

/**
 * Sets a task status from user.
 * 
 * @see - skrivStatus()
 */
void Task::settStatus()
{
    // A menu
    cout << "Select a status and press ENTER\n" << '\n';
    cout << "Task Status: " << '\n';
    cout << "\t1) Finished" << '\n';
    cout << "\t2) In progress" << '\n';
    cout << "\t3) Not started" << '\n';
    cout << '\n';
    oppgaveStatus = lesInt("Command", 1, 3);
    skrivStatus(); // Writes the current status on screen
    cout << '\n';
}

/**
 * Sets a task description from user.
 */
void Task::settBeskrivelse()
{
    cout << "Write a task description and press ENTER\n" << '\n';
    cout << "Task Description: "; getline(cin, oppgaveBeskrivelse);
}

/**
 * Writes the tasks name on screen.
 */
void Task::skrivNavn()
{
    cout << oppgaveNavn;
}

/**
 * Writes the oriority status on screen.
 * 
 * @see - hentPrioritetNavn()
 */
void Task::skrivPrioritet() const
{
    cout << "\tTask Priority:  " << hentPrioritetNavn() << '\n';
                                    //Returns prioriety name
}

/**
 * Gives the user a menu where specific data can be changed.
 * 
 * @see - skrivEndreDataMeny()
 * @see - endreNavn()
 * @see - endrePrioritet()
 * @see - endreDato()
 * @see - endreStatus()
 * @see - endreBeskrivelse()
 */
void Task::endreData()
{
    skrivEndreDataMeny(); // Writes the change menu
    char command = lesChar("Command");
    switch (command)
    {
    case 'N': endreNavn(); break; // Changes name
    case 'P': endrePrioritet(); break; // Changes priority
    case 'D': endreDato(); break; // Changes date
    case 'S': endreStatus(); break; // Changes status
    case 'T': endreBeskrivelse(); break; // Changes description
    case 'E': break; // Quits the menu
    default: cout << "Please enter a valid input\n\n"; break;
    }
}

/**
 * Writes the tasks data on screen.
 * 
 * @see - skrivPrioritet()
 * @see - skrivStatus()
 */
void Task::skrivData() const
{
    cout << "\tName:  " << oppgaveNavn << '\n';
    skrivPrioritet(); // Shows task priority
    cout << "\tDue Date:  " << dag << "." << maaned << "." << aar << '\n';
    skrivStatus(); // Shows task status
    cout << "\tDescription:  " << oppgaveBeskrivelse << '\n';
}

/**
 * Writes the status of the task.
 * 
 */
void Task::skrivStatus() const
{
    cout << "\tTask Status:  ";
    switch (oppgaveStatus)
    {
    case 1: cout << "Finished"; break;
    case 2: cout << "In progress"; break;
    case 3: cout << "Not started"; break;
    default: break;
    }
    cout << '\n';
}

/**
 * Changes the name of the task.
 */
void Task::endreNavn()
{
    string nyttNavn;
    cout << "Write a name and press ENTER\n" << '\n';
    cout << "Task Name: "; getline(cin, nyttNavn);
    cout << '\n';
    // Feedback to the user
    cout << "Name changed from \"" << oppgaveNavn << "\" to \"" << nyttNavn;
    cout << "\" " << '\n';
    oppgaveNavn = nyttNavn;
}

/**
 * Changes priority of the task.
 * 
 * @see - hentPrioritetNavn()
 * @see - hentPrioritetNavn(const int i)
 */
void Task::endrePrioritet()
{
    // A menu
    cout << "Select a priority and press ENTER\n" << '\n';
    cout << "Priority:  " << '\n';
    cout << "\t1) High" << '\n';
    cout << "\t2) Medium" << '\n';
    cout << "\t3) Low" << '\n';
    cout << '\n';
    int nyPrioritet = lesInt("Command", 1, 3); // Reads an int from user

    // Feedback to the user
    cout << "Priority changed from \"" << hentPrioritetNavn()
         << "\" to \"" << hentPrioritetNavn(nyPrioritet);
    cout << "\"" << '\n';
    oppgavePrioritet = nyPrioritet;

    cout << '\n';
}

/**
 * Returns the name of the task.
 * 
 * @return - task priority name ("1 = High", "2 = Medium", "3 = Low")
 */
string Task::hentPrioritetNavn() const
{
    switch (oppgavePrioritet)
    {
    case 1: return "High"; break;
    case 2: return "Medium"; break;
    case 3: return "Low"; break;
    default: break;
    }
}

/**
 * Returns the name of the task based on integer (1-3).
 *
 * @param i - the task priority number
 * @return  - task priority name ("1 = High", "2 = Medium", "3 = Low")
 */
string Task::hentPrioritetNavn(const int i) const
{
    switch (i)
    {
    case 1: return "High"; break;
    case 2: return "Medium"; break;
    case 3: return "Low"; break;
    default: break;
    }
}

/**
 * Returns the priority number of the task.
 *
 * @return - task priority number ("1 = High", "2 = Medium", "3 = Low")
 */
int Task::hentPrioritetNummer()
{
    return oppgavePrioritet;
}

/**
 * Changes the task date.
 */
void Task::endreDato()
{
    // Inputs new date from user
    int nyDag, nyMaaned, nyttAar;
    cout << "\nWrite a date and press ENTER\n" << '\n';
    nyDag = lesInt("\tDay", 1, 31);
    nyMaaned = lesInt("\tMonth", 1, 12);
    nyttAar = lesInt("\tYear", 2000, 3000);

    // Feedback to the user
    cout << "\nDate changed from \"" << dag << "." << maaned << "." << aar
        << "\" to \"" << nyDag << "." << nyMaaned << "." << nyttAar << "\""
        << '\n';

    dag = nyDag;
    maaned = nyMaaned;
    aar = nyttAar;
}

/**
 * Changes the task status.
 * 
 * @see - hentStatusNavn()
 * @see - hentStatusNavn(const int i)
 */
void Task::endreStatus()
{
    // A menu
    cout << "Select a status and press ENTER\n" << '\n';
    cout << "\t1) Finished" << '\n';
    cout << "\t2) In progress" << '\n';
    cout << "\t3) Not started" << '\n';
    cout << '\n';
    int nyStatus = lesInt("Command", 1, 3);

    // Feedback to the user
    cout << "Status changed from \"" << hentStatusNavn()
        << "\" to \"" << hentStatusNavn(nyStatus);
    cout << "\"" << '\n';
    oppgaveStatus = nyStatus;

    cout << '\n';

}

/**
 * Returns the status name of the task.
 * 
 * @return - task priority status name ("1 = Finished", "2 = In progress",
 *                                      "3 = Not started")
 */
string Task::hentStatusNavn() const
{
    switch (oppgaveStatus) // Returns values based on oppgaveStatus
    {
    case 1: return "Finished"; break;
    case 2: return "In progress"; break;
    case 3: return "Not started"; break;
    default: break;
    }
    cout << '\n';
}

/**
 * Returns the status name of task based on integer (1-3).
 * 
 * @param i - the task status number
 * @return - task priority status name ("1 = Finished", "2 = In progress",
 *                                      "3 = Not started")
 */
string Task::hentStatusNavn(const int i) const
{
    switch (i)
    {
    case 1: return "Finished"; break;
    case 2: return "In progress"; break;
    case 3: return "Not started"; break;
    default: break;
    }
    cout << '\n';
}

/**
 * Changes the description of the task.
 */
void Task::endreBeskrivelse()
{
    string nyBeskrivelse;
    cout << "Write a task description and press ENTER\n" << '\n';
    cout << "Task Description: "; getline(cin, nyBeskrivelse);

    cout << "Description changed from \"" << oppgaveBeskrivelse
        << "\" to \"" << nyBeskrivelse;
    cout << "\"" << '\n';
    oppgaveBeskrivelse = nyBeskrivelse;

    cout << '\n';
}

/**
 * Writes the task information to a file.
 * 
 * @param ut - the file to be written to
 */
void Task::skrivTilFil(ofstream & ut) const
{
     ut << "T " << oppgaveNavn << '\n' << oppgavePrioritet << '\n'
        << dag << " " << maaned << " " << aar << '\n' << oppgaveStatus << '\n'
        << oppgaveBeskrivelse << '\n';
}

/**
 * Constructs a task from a file.
 * 
 * @param inn - the file to be read from
 */
Task::Task(ifstream& inn)
{
    getline(inn, oppgaveNavn);
    inn >> oppgavePrioritet; inn.ignore();
    inn >> dag; inn.ignore();
    inn >> maaned; inn.ignore();
    inn >> aar; inn.ignore();
    inn >> oppgaveStatus; inn.ignore();
    getline(inn, oppgaveBeskrivelse);
}

/**
 * Changes a selected task.
 * 
 * @see listOppgaverForenklet()
 * @see Task::skrivData()
 * @see Task::endreData()
 * @see pressEnter()
 */
void endreEnOppgave()
{
    // If there are tasks registered
    if (gOppgaver.size() > 0)
    {

        listOppgaverForenklet(); // Creates a simplified list of the tasks
        int oppgaveNummer = lesInt("Command ", 0, gOppgaver.size());

        if (oppgaveNummer > 0) // If you select 0 you break out of the function
        {
            gOppgaver[oppgaveNummer - 1]->skrivData(); // Writes task data
            cout << '\n';
            gOppgaver[oppgaveNummer-1]->endreData(); // Changes task data
        }
    }
    else cout << "You currently have no registered tasks" << '\n';

    pressEnter(); // Press enter to continue
}

/**
 * Creates new tasks from file data.
 * 
 * @see Task::Task(ifstream & inn)
 */
void lesFraFil()
{
    //int oppgaveNummer;
    Task* nyTask = nullptr;

    // Reads from file
    ifstream innfilB("output.txt");
    char type; // Makes sure what the program is about to read is a file

    // If the file exists
    if (innfilB)
    {
        innfilB >> type;
        innfilB.ignore();
        while (!innfilB.eof())
        {
            sisteNr++;
            nyTask = new Task(innfilB);
            gOppgaver.push_back(nyTask); // Puts new task in vector container

            innfilB >> type;
            innfilB.ignore();
        }
        innfilB.close();
    }
}

/**
 *  Creates a new task.
 *
 *  @see - int main() - switch case N.
 *  @see - pressEnter()
 */
void nyOppgave()
{

    cout << "You have chosen \"New Task\"\n" << '\n';

    sisteNr++;

    Task* nyTask = nullptr;

    nyTask = new Task();

    nyTask->lesData(); // Objects data is read
    cout << '\n';
    gOppgaver.push_back(nyTask); // Puts it into the vector container
    cout << "Added task: \n" << '\n';
    nyTask->skrivData(); // Writes data on screen
    pressEnter(); // Press enter to continue
}

/**
 *  Writes data of specific tasks.
 *
 *  @see - skrivSorteringsMenu()
 *  @see - skrivUtAlleOppgaver()
 *  @see - skrivUtMedPrioritetfilter()
 *  @see - pressEnter()
 */
void skrivAlleOppgaver()
{
    // If there are no tasks the user is informed
    if(gOppgaver.size() == 0)
    {
        cout << "You currently have no registered tasks";
    }
    else
    {
        skrivSorteringsMeny();
        char valg = lesChar("Command");
        switch (valg)
        {
        case 'A': skrivUtAlleOppgaver();  break; // Write all data of all tasks
        case 'H': skrivUtMedPrioritetfilter(1); break; // Only of a priority
        case 'M': skrivUtMedPrioritetfilter(2); break; // Only of a priority
        case 'L': skrivUtMedPrioritetfilter(3); break; // Only of a priority
        case 'E': break;
        default: cout << "\n Inavlid input" << '\n'; break;
        }
    }

    pressEnter();
}

/**
 * Writes all information of all the tasks on screen.
 * 
 * @see virtual Task::skrivData()
 */
void skrivUtAlleOppgaver()
{
    // A range based for loop that goes through all tasks in gOppgaver
    int num = 1; // What task number the loop is on
    for (const auto& val : gOppgaver)
    {
        cout << "\nTask Number: " << num << '\n';
        val->skrivData();
        num++;
    }
    cout << "\n\n";
}

/**
 * Writes all onformation of all tasks with a specific priority.
 * 
 * @param valg - the priority number of the tasks
 * @see - Task::hentPrioritetNummer()
 * @see - Task::skrivData()
 */
void skrivUtMedPrioritetfilter(const int valg)
{
    // A range based for loop that goes through all tasks in gOppgaver
    int num = 1; // What task number the loop is on
    for (const auto& val : gOppgaver)
    {
        if (valg == val->hentPrioritetNummer()) // If the priority is correct
        {
            cout << "\nTask Number: " << num << '\n';
            val->skrivData(); // Writes the tasks data on screen
        }
        num++;
    }
    cout << "\n\n";
}

/**
 * Writes the menu of the choices the user have when it comes to sorting.
 */
void skrivSorteringsMeny()
{
    cout << "\nWhat tasks do you want to view?\n"
        << "\tA - All tasks\n"
        << "\tH - High priority\n"
        << "\tM - Medium priority\n"
        << "\tL - Low priority\n"
        << "\tE - Exit to menu\n\n";
}

/**
 * Lists the tasks with just number and names.
 * 
 * @see - Task::skrivNavn()
 */
void listOppgaverForenklet()
{
    if (gOppgaver.size() == 0)
    {
        cout << "There are no Tasks.";
    }
    else
    {
        cout << "Choose a task and press ENTER" << '\n';
        cout << "0) Exit to menu" << '\n';
        // A range based for loop that goes through all tasks in gOppgaver
        int num = 1; // What task number the loop is on
        for (const auto& val : gOppgaver)
        {
            cout << num << ") ";
            val->skrivNavn(); // Writes the name of the task
            cout << '\n';
            num++;
        }
        cout << '\n';
    }
}


/**
 *  Writes the programs main menu on screen.
 */
void skrivMeny()
{
    cout << "\nMenu Choices:\n"
         << "\tN - New Task\n"
         << "\tV - View Tasks\n"
         << "\tE - Edit Task\n"
         << "\tD - Delete a Task\n"
         << "\tQ - Quit / Avslutt\n\n";
}

/**
 * Writes the programs change data menu on screen.
 */
void skrivEndreDataMeny()
{
    cout << "What do you want to change?\n\n"
        << "\tN - Name\n"
        << "\tP - Priority\n"
        << "\tD - Due Date\n"
        << "\tS - Status\n"
        << "\tT - Task description\n"
        << "\tE - Exit\n\n";
}

/**
 *  Skriver hele datastrukturen til fil.
 *
 *  @see - virtual Task::skrivTilFil(...)
 */
void skrivTilFil()
{
    // If there are no tasks the program informs the user
    if(gOppgaver.empty())
    {
        cout << "There are no Tasks.";
    }
    else
    {
         ofstream utfilB("output.txt");   //  Fileobjekt for PRINTING.

         //Sees if the file is opened
         if(utfilB.is_open())
         {
            // A range based for loop that goes through all tasks in gOppgaver
            for (const auto & val : gOppgaver)
            {
                val->skrivTilFil(utfilB); 
                 //All tasks writes their own data themselves
            }

            cout << "\n\n";
            // Closes file
            utfilB.close();
         }

    }
}

/**
 * Makes the user have to press enter to continue.
 */
void pressEnter()
{
    cout << "\nPress ENTER to continue ";
    // Ignores everything that isn't ENTER
    cin.ignore(std::numeric_limits<streamsize>::max(), '\n');
}

/**
 *  Sletter en oppgave
 *
 *  @see - listOppgaverForenklet()
 *  @see - int main() - switch case D.
 */
void slettOppgave()
{
    //Variable is used in nyOppgave
    //Used to go through gOppgaver.
    int oppgaveNummer;

    if (gOppgaver.size() > 0)
    {
        listOppgaverForenklet(); // Lists tasks with name and number

        oppgaveNummer = lesInt("Command", 0, gOppgaver.size());
        if (oppgaveNummer > 0)
        {
            // If the user confirms the deletion
            if (confirmDeletion())
            {
                // Ereases the task from vector
                gOppgaver.erase(gOppgaver.begin() + oppgaveNummer-1);

                cout << "Task successfully deleted\n" << '\n';
            }
            else // Else the user is informed
                cout << "Cancelled deletion\n" << '\n';

            pressEnter(); // Press enter to continue
        }
        //om nummeret ikke er brukt.
        else if (oppgaveNummer == 0)
        {
            pressEnter(); // Press enter to continue
        }
    }
    else // If you have no tasks
    {
        cout << "You currently have no registered tasks\n" << '\n';
        pressEnter();
    }
}

/**
 * Returns if the user REALLY wants to delete the task.
 * 
 * @return - true or false (yes or no)
 */
bool confirmDeletion()
{
    cout << "Are you sure you want to delete chosen task?" << "\n"
        << "y - yes" << "\n"
        << "n - no" << '\n';
    return lesChar("Command") == 'Y'; // True if yes, false if anything else
}
